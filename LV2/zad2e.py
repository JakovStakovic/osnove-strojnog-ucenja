import numpy as np
import matplotlib.pyplot as plt

data = np.genfromtxt('data.csv', delimiter=',')

men = (data[:,0] ==1)
women = (data[:,0] ==0)

men_height = data[men, 1]
women_height = data[women, 1]

max_height_men = np.max(men_height)
max_height_women = np.max(women_height)
avg_height_men = np.mean(men_height)
avg_height_women = np.mean(women_height)
min_weight_men = np.min(men_height)
min_weight_women = np.min(women_height)
avg_height_men = np.mean(men_height)
avg_height_women = np.mean(women_height)

print(max_height_men)
print(max_height_women)
print(avg_height_men)
print(avg_height_women)
print(min_weight_men)
print(min_weight_women)
print(avg_height_men)
print(avg_height_women)


