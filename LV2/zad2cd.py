import numpy as np
import matplotlib.pyplot as plt
#for every 50th person
#Izraˇcunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom
#podatkovnom skupu.

data = np.genfromtxt("data.csv", delimiter=",")
lowest_height = np.min(data[1:,1])
lowest_weight = np.min(data[1:,2])
max_height = np.max(data[1:,1])
max_weight = np.max(data[1:,2])
every_fifth_height = data[1::50, 1]
every_fifth_weight = data[1::50, 2]
avg_height = np.mean(data[1:,1])
avg_weight = np.mean(data[1:,2])
plt.scatter(every_fifth_height, every_fifth_weight, c=data[1::50,0], cmap='RdYlBu', alpha=0.5)
plt.xlabel('Masa (kg)')
plt.ylabel('Visina (cm)')
print(lowest_height)
print(lowest_weight)
print(max_height)
print(max_weight)
print(avg_height)
print(avg_weight)
plt.grid(True)
plt.show()

#Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili
#muškarce, stvorite polje koje zadrži bool vrijednosti i njega koristite kao indeks retka.
#ind = (data[:,0] == 1)

