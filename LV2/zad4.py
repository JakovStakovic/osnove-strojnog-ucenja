import numpy as np
import matplotlib.pyplot as plt

black = np.zeros((50,50))
white = np.ones((50,50))

combined = np.concatenate((black, white), axis=1)
reversed = np.fliplr(combined)
matrix = np.vstack((reversed, combined))

plt.imshow(matrix, cmap='Greys')
plt.show()