import numpy as np
import matplotlib.pyplot as plt


# Create a square with side length 2 centered at (0, 0)
side_length = 2
x = np.array([1,3,3,2,1])
y = np.array([1,1,2,2,1])

# Plot the square
plt.plot(x, y, linewidth=1, color="red", marker=".", markersize=10)
plt.axis('equal')  # Ensure equal aspect ratio
plt.xlabel('X')
plt.ylabel('Y')
plt.title('Square')
plt.xlim(0,4)
plt.ylim(0,4)
plt.grid(True)
plt.show()


class sklearn.
