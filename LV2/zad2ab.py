import numpy as np
import matplotlib.pyplot as plt

#Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
#ženama. Skripta zadatak_2.py uˇcitava dane podatke u obliku numpy polja data pri ˇcemu je u
#prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a tre´ci
#stupac polja je masa u kg.

data = np.genfromtxt("data.csv", delimiter=",")
visina = data[1:,1]
masa = data[1:,2]
length = len(data[1:,0])
print("Mjereno je za ", length," osoba")
plt.scatter(masa, visina, c=data[1:,0], cmap='RdYlBu', alpha=0.5)
plt.xlabel('Masa (kg)')
plt.ylabel('Visina (cm)')
plt.grid(True)
plt.show()