import pandas as pd 

data = pd.read_csv("LV3/data.csv")
#Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili ˇ
#duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip 
#category

print(f"Length of data: {len(data)}")
print(f"Type of data:\n{data.dtypes}")
print(f"Missing values:\n{data.isnull().sum()}")
print(f"Duplicated values:\n{data.duplicated().sum()}")

for col in ['Make', 'Model', 'Vehicle Class', 'Transmission', 'Fuel Type']:
    data[col] = data[col].astype('category')

print(data.dtypes)

#Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal:
#ime proizvodaca, model vozila i kolika je gradska potrošnja.
print(data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False).head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print(data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False).tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija 
#plinova za ova vozila?
number = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print(f"Vehicles with engine size between 2.5 and 3.5 L: {number['Make'].count()}")
print(f"Average C02 emissions for vehicles with engine size between 2.5 and 3.5 L: {number['CO2 Emissions (g/km)'].mean()}")

#Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02
#plinova automobila proizvodaca Audi koji imaju 4 cilindara?

print(f"Number of Audi vehicles: {data[data['Make'] == 'Audi']['Model'].count()}")
print(f"Average C02 emissions for Audi vehicles with 4 cylinders: {data[(data['Make'] == 'Audi') & (data['Cylinders'] == 4)]['CO2 Emissions (g/km)'].mean():.2f}")

#) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na
#broj cilindara?

print(f"Vehicles with even number of cylinder: {data[data['Cylinders'] % 2 == 0]['Make'].count():.2f}")
print(f"Average C02 emissions for vehicles with even number of cylinders: {data[data['Cylinders'] % 2 == 0]['CO2 Emissions (g/km)'].mean():.2f}")

#Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila 
#koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?

print(f"Average fuel consumption for vehicles with diesel engine: {data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].mean():.2f}")
print(f"Average fuel consumption for vehicles with regular engine: {data[data['Fuel Type'] == 'X']['Fuel Consumption City (L/100km)'].mean():.2f}")
print(f"Median fuel consumption for vehicles with diesel engine: {data[data['Fuel Type'] == 'D']['Fuel Consumption City (L/100km)'].median():.2f}")
print(f"Median fuel consumption for vehicles with regular engine: {data[data['Fuel Type'] == 'X']['Fuel Consumption City (L/100km)'].median():.2f}")

#Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?
print(f"Vehicle with 4 cylinders and diesel engine with highest fuel consumption: {data.sort_values(by='Fuel Consumption City (L/100km)', ascending=False)[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')]['Model'].head(1)}")

#Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?
print(f"Number of vehicles with manual transmission: {data[data['Transmission'].str.startswith('M')]['Model'].count()}")

# Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat. 
print(f"{data.corr(numeric_only=True)}")