import pandas as pd
import matplotlib.pyplot as plt
#Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.

data = pd.read_csv("LV3/data.csv")

plt.figure()
data['CO2 Emissions (g/km)'].plot(kind='hist', bins=20)
plt.show()

#Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije ¯
#C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu¯
#velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva   

plt.figure()
color = {'X': 'blue', 'D': 'red', 'E': 'green', 'Z': 'yellow', 'N': 'black'}
colored_fuel = data['Fuel Type'].replace(color)
data.plot(x='Fuel Consumption City (L/100km)', y='CO2 Emissions (g/km)', kind='scatter', c=colored_fuel, colormap='viridis')
plt.show()