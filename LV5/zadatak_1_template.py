import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.datasets import make_classification 
from sklearn.model_selection import train_test_split

X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

#Prikažite podatke za ucenje u x1 −x2 ravnini matplotlib biblioteke pri cemu podatke obojite ˇ
#s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
#marker (npr. ’x’). Koristite funkciju scatter koja osim podataka prima i parametre c i
#cmap kojima je moguce definirati boju svake klase.

plt.scatter(X_train[:,0], X_train[:,1], c=y_train, cmap=plt.cm.colors.ListedColormap(['red', 'blue']))
plt.scatter(X_test[:,0], X_test[:,1], c=y_test, marker='x', cmap=plt.cm.colors.ListedColormap(['red', 'blue']))
plt.show()

#Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa podataka za ucenje

LogisticRegression_model = LogisticRegression()
LogisticRegression_model.fit(X_train, y_train)

#Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke ¯
#naucenog modela u ravnini x1 − x2 zajedno s podacima za ucenje. Napomena: granica ˇ
#odluke u ravnini x1 −x2 definirana je kao krivulja: θ0 + θ1x1 + θ2x2 = 0.

theta0 = LogisticRegression_model.intercept_
theta1 = LogisticRegression_model.coef_[0,0]
theta2 = LogisticRegression_model.coef_[0,1]

#Provedite klasifikaciju skupa podataka za testiranje pomocu izgra ´ denog modela logisti ¯ cke ˇ
#regresije. Izracunajte i prikažite matricu zabune na testnim podacima. Izra ˇ cunate to ˇ cnost, ˇ
#preciznost i odziv na skupu podataka za testiranje.

y_pred = LogisticRegression_model.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print("Accuracy: {:.2f}%".format(accuracy * 100))
print(classification_report(y_test, y_pred))

cm = confusion_matrix(y_test, y_pred, labels=[0, 1])
disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=[0, 1])
disp.plot()


#Prikažite skup za testiranje u ravnini x1 −x2. Zelenom bojom oznacite dobro klasi ˇ ficirane
#primjere dok pogrešno klasificirane primjere oznacite crnom bojom. 

plt.figure()
y_correct = y_pred == y_test
plt.scatter(X_test[:,0], X_test[:,1], c=y_correct, cmap=plt.cm.colors.ListedColormap(['red', 'blue']))
plt.show()