#Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji ˇ
#sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
#potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
#vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
#(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovaraju ˇ cu poruku.

lista = []
while True:
    unos = input("Enter value:")
    if(unos == "Done"):
        break
    try:
        unos = float(unos)
        lista.append(unos)
    except:
        print("Wrong value")
        continue
print(f"Number of values: {len(lista)}")
print(f"Average: {sum(lista) / len(lista)}")
print(f"Minimum: {min(lista)}")
print(f"Maximum: {max(lista)}")
