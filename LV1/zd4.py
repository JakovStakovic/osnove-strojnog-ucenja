#Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva song.txt.
#Potrebno je napraviti rjecnik koji kao kljuceve koristi sve razlicite rijeci koje se pojavljuju u 
#datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (kljuc) pojavljuje u datoteci. 
#Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

fhand = open("song.txt")
dict = {}
for line in fhand:
    line = line.rstrip()
    line = line.lower()
    words = line.split()
    for word in words:
        if word not in dict:
            dict[word] = 1
        else:
            dict[word] += 1
print(dict)
fhand.close()