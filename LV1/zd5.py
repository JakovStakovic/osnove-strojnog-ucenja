#Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva SMSSpamCollection.txt
#[1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke oznacene kao spam, a neke kao ham.
#Primjer dijela datoteke:
#ham Yup next stop.
#ham Ok lar... Joking wif u oni...
#spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff!
#a) Izracunajte koliki je prosjecan broj rijeci u SMS porukama koje su tipa ham, a koliko je 
#prosjecan broj rijeci u porukama koje su tipa spam. 
#b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?

fhand = open("SMSSpamCollection.txt")
countHam = 0
countSpam = 0
count = 0
for line in fhand:
    line = line.rstrip()
    line = line.lower()
    words = line.split()
    for word in words:
        if words[0] == "ham":
            countHam +=1
        elif words[0] == "spam":
            countSpam +=1
            if(words[-1] == "!"):
                count += 1
print("Avg number of words in ham: ", float(countHam/5574), "Avg number of words inspam: ", float(countSpam/5574))
print("Number of words ending with !:", count)
