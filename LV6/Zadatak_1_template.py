import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import cross_validate
def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
    np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl)


# ucitaj podatke
data = pd.read_csv("LV6/Social_Network_Ads.csv")
print(data.info())

data.hist()
plt.show()

# dataframe u numpy
X = data[["Age","EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

# podijeli podatke u omjeru 80-20%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

# skaliraj ulazne velicine
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform((X_test))

# Model logisticke regresije
LogReg_model = LogisticRegression(penalty=None) 
LogReg_model.fit(X_train_n, y_train)

# Evaluacija modela logisticke regresije
y_train_p = LogReg_model.predict(X_train_n)
y_test_p = LogReg_model.predict(X_test_n)

print("Logisticka regresija: ")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p))))

# granica odluke pomocu logisticke regresije
plot_decision_regions(X_train_n, y_train, classifier=LogReg_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
plt.tight_layout()
plt.show()

#Izradite algoritam KNN na skupu podataka za ucenje (uz ˇ K=5). Izracunajte tocnost 
#klasifikacije na skupu podataka za ucenje i skupu podataka za testiranje. Usporedite 
#dobivene rezultate s rezultatima logisticke regresije. Što primjecujete vezano uz dobivenu
#granicu odluke KNN modela?
#Kako izgleda granica odluke kada je K = 1 i kada je K = 100

KNN_model = KNeighborsClassifier(n_neighbors=100)
KNN_model.fit(X_train_n, y_train)
y_train_p_knn = KNN_model.predict(X_train_n)


plot_decision_regions(X_train_n, y_train, classifier=KNN_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_knn))))
plt.tight_layout()
plt.show()

print("KNN:")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, KNN_model.predict(X_train_n)))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, KNN_model.predict(X_test_n)))))
print("Prilikom mijenjanja K u KNN modelu, zakljucio sam kako se K ovisi o tocnosti modela. Sto je veci K to je tocnost modela manja, dok ako je K=1 tocnost je priblizno 100%.")



#Pomocu unakrsne validacije odredite optimalnu vrijednost hiperparametra K algoritma KNN za podatke iz Zadatka 1.
param_grid = {'n_neighbors': range(1, 21)}  # Isprobajmo K od 1 do 20

grid_search = GridSearchCV(KNN_model, param_grid, cv=5)

grid_search.fit(X_train, y_train)

print("Najbolja vrijednost hiperparametra K:", grid_search.best_params_['n_neighbors'])

#Na podatke iz Zadatka 1 primijenite SVM model koji koristi RBF kernel funkciju
#te prikažite dobivenu granicu odluke. Mijenjajte vrijednost hiperparametra C i γ. Kako promjena
#ovih hiperparametara utjece na granicu odluke te pogrešku na skupu podataka za testiranje? ˇ
#Mijenjajte tip kernela koji se koristi. Što primjecujete? 

SVM_model = svm.SVC(kernel='rbf', C=1, gamma=1)
SVM_model.fit(X_train_n, y_train)
y_train_p_svm = SVM_model.predict(X_train_n)

plot_decision_regions(X_train_n, y_train, classifier=SVM_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p_svm))))
plt.tight_layout()
plt.show()

print("SVM: ")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, SVM_model.predict(X_train_n)))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, SVM_model.predict(X_test_n)))))
print("Prilikom mijenanja C i gamma, zakljucio sam sto su veci C i gamma to je tocnost modela bolja, ali tocnost testa opada.")

#Pomocu unakrsne validacije odredite optimalnu vrijednost hiperparametra C i γ algoritma SVM za problem iz Zadatka 1.

param_grid = {'C': [0.1, 1, 10, 100],
              'gamma': [0.001, 0.01, 0.1, 1]}
grid_search = GridSearchCV(SVM_model, param_grid, cv=5)

grid_search.fit(X_train, y_train)

print("Najbolji hiperparametri:", grid_search.best_params_)