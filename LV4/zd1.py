import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
import matplotlib.pyplot as plt
import math



#Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite
#podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.

data = pd.read_csv("LV4/data.csv")
variables = ['Fuel Consumption City (L/100km)',
            'Fuel Consumption Hwy (L/100km)',
            'Fuel Consumption Comb (L/100km)',
            'Fuel Consumption Comb (mpg)',
            'Engine Size (L)',
            'Cylinders']

X = data[variables]
y = data['CO2 Emissions (g/km)']

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2)

#Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova 
#o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za ucenje oznacite 
#plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom. 

plt.figure()
plt.xlabel('Input variable')
plt.ylabel('CO2 Emissions (g/km)')
plt.scatter(x = X_train['Fuel Consumption Hwy (L/100km)'], y = y_train, color = 'blue')
plt.scatter(x = X_test['Fuel Consumption Hwy (L/100km)'], y = y_test, color = 'red')

#Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti 
#jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih parametara skaliranja 
#transformirajte ulazne velicine skupa podataka za testiranje.

sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

plt.figure()
plt.xlabel('Input variable')
plt.hist(x = X_train['Fuel Consumption Hwy (L/100km)'], color = 'blue', bins=5)


plt.figure()
plt.xlabel('Input variable')
plt.hist(x = X_train_n[:,1], color = 'red', bins=5)


#Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i
#povežite ih s izrazom 4.6.

linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

#Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite 
#pomocu dijagrama raspršenja odnos izmedu stvarnih vrijednosti izlazne velicine i procjene 
#dobivene modelom

y_pred = linearModel.predict(X_test_n)

plt.figure()
plt.xlabel('Actual CO2 Emissions (g/km)')
plt.ylabel('Predicted CO2 Emissions (g/km)')
plt.scatter(x = y_test, y = y_pred)

MSE = mean_squared_error(y_test , y_pred)
MAE = mean_absolute_error( y_test , y_pred )
RMSE = math.sqrt(MSE)
MAPE = mean_absolute_percentage_error(y_test , y_pred)
R2 = r2_score(y_test , y_pred)
print(f"MSE: {MSE}, MAE: {MAE}, RMSA: {RMSE}, MAPE: {MAPE}, R2: {R2}")
plt.show()
