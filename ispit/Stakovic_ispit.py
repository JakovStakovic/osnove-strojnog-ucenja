#import bibilioteka
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
from sklearn.metrics import accuracy_score, recall_score, precision_score
from tensorflow import keras
from keras import layers
import seaborn as sn
from sklearn.linear_model import LinearRegression
import math
from sklearn.metrics import mean_absolute_error, mean_squared_error, mean_absolute_percentage_error, r2_score
##################################################
#1. zadatak
##################################################
data = pd.read_csv("ispit/winequality-red.csv", sep=';')

#a)
print("----")
print("Broj mjerenja: ", len(data))
#b)
alcohol = data['alcohol']
plt.hist(x=alcohol, bins=20)
plt.xlabel('Alcohol %')
plt.ylabel('Količina')
plt.title("Distribucija alkoholne jakosti")
plt.show()
#c)
print("-----")
number_lower = data[(data['quality']<6)]
print("Broj manji od 6: ", len(number_lower))
number_higher = data[(data['quality']>=6)]
print("Broj veci od 6: ", len(number_higher))
#d)
corr_matrix = data.corr()
sn.heatmap(corr_matrix, annot=True)
plt.show()

##################################################
#2. zadatak
##################################################
print("------")
X = data[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides', 'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = data['quality']
y[y<6] = 0
y[y>=6] = 1

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

scaler = StandardScaler()
X_train = scaler.fit_transform(X_train)
X_test = scaler.transform(X_test)

#a)
model = LinearRegression()
model.fit(X_train, y_train)
print(model.coef_)
print("--------")
#b)
y_test_pred = model.predict(X_test)
plt.xlabel('Stvarne izlazne vrijednosti')
plt.ylabel('Prediktane izlazne vrijednosti')
plt.scatter(x = y_test, y = y_test_pred, color='blue', alpha=0.8)
plt.show()
#c)
MSE = mean_squared_error(y_test , y_test_pred)
MAE = mean_absolute_error( y_test , y_test_pred )
RMSE = math.sqrt(MSE)
MAPE = mean_absolute_percentage_error(y_test , y_test_pred)
R2 = r2_score(y_test , y_test_pred)
print(f"MSE: {MSE}, MAE: {MAE}, RMSA: {RMSE}, MAPE: {MAPE}, R2: {R2}")



##################################################
#3. zadatak
##################################################
print("------")
X = data[['fixed acidity', 'volatile acidity', 'citric acid', 'residual sugar', 'chlorides', 'free sulfur dioxide', 'total sulfur dioxide', 'density', 'pH', 'sulphates', 'alcohol']]
y = data['quality']
y[y<6] = 0
y[y>=6] = 1

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

#a)
print("---------------------------------")
model = keras.Sequential()
model.add(layers.Input(shape=(11,)))  # 11 ulaznih varijabli
model.add(layers.Dense(22, activation='relu'))
model.add(layers.Dense(12, activation='relu'))
model.add(layers.Dense(4, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))

print(model.summary())

#b)
print("---------------------------------")
model.compile(optimizer='adam',
                    loss='binary_crossentropy', 
                    metrics=['accuracy'])
#c)
print("---------------------------------")
model.fit(X_train,
                y_train,
                epochs = 400,
                batch_size = 50,
                validation_split = 0.1)
#d)
model.save('model.h5')
#model = load_model('model.h5')

#e)
score = model.evaluate(X_test, y_test)
print("Evaluacija na testnom skupu: " + str(score))

#f)
y_test_pred = model.predict(X_test)

y_test_binary  = np.where(y_test_pred > 0.5, 1, 0)

cm = confusion_matrix(y_test, y_test_binary)
disp = ConfusionMatrixDisplay(cm)
disp.plot()
plt.show()
